require('dotenv-safe').load();

var express    = require('express');
var chalk      = require('chalk');
var app        = require('express')();
var http       = require('http').Server(app);
var io         = require('socket.io')(http);
var foncy      = require('foncy-nonce');
var moment     = require('moment');
var bodyParser = require('body-parser');
var Redis      = require('ioredis');
var twilio     = require('twilio')(process.env.TWILIO_USER, process.env.TWILIO_PASS);
var session    = require('express-session');
var helmet     = require('helmet');
var RedisStore = require('connect-redis')(session);

const { createLogger, format, transports } = require('winston');
const { combine } = format;

Redis.Promise.onPossiblyUnhandledRejection(function (error) {
  console.error("UH OH REDIS ERROR!");
  console.error(error.command.name, error.command.args);
});

var redis = new Redis();

var sess = {
  store: new RedisStore({client: redis}),
  secret: process.env.SESSION_SECRET,
  resave: false,
  saveUninitialized: true,
  cookie: { secure: false },
};

var tports = [new transports.Console()];
if (app.get('env') === 'production') {
  winston.info('running in production mode.');
  tports.push(new transports.File({ filename: process.env.LOG_PATH }))
}

const winston = createLogger({
  format: combine(
    format.splat(),
    format.colorize(),
    format.timestamp(),
    format.printf(info => `[${moment().format('YYYY-MM-DD H:mm')}] ${info.level}: ${info.message}` ),
  ),
  transports: tports});

if (app.get('env') === 'production') {
  app.set('trust proxy', 1) // trust first proxy
  sess.cookie = { secure: true, sameSite: 'lax' };
} else {
  winston.level = 'debug';
}

app.use(helmet()); // basic security headers
app.use(session(sess));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(__dirname + '/dist', {index: false}));

var iceServersCache = {
  value: undefined,
  updated: 0
}

app.get('/', (req, res) => {
  let noncy = foncy.generate();
  res.redirect(`/${noncy}`);
});

/*app.get('/herg_roulette', async (req, res) => {
  // check for open hugs
  available_room = await redis.rpop('roulette_rooms:open');
  if (available_room) {
    res.redirect(`/${available_room}`);
  } else { // open new roulette room
    let noncy = foncy.generate();
    redis.set(`room:${noncy}:roulette`, '1', 'ex', 2592000)
    res.redirect(`/${noncy}`);
  }
});*/

app.get('/grab_guid', (req, res) => {
  let noncy = foncy.generate();
  res.send(noncy);
});

app.get('/known_hergs', (req, res) => {
  let noncy = foncy.generate();
  res.redirect(`/${noncy}`);
});

app.get('/contact_erdress', (req, res) => {
  res.send('hergs@kermpany.com');
});

app.get('/stats', (req, res) => {
  res.sendFile(__dirname +  '/dist/stats.html');
});

app.get('/:room([a-zA-Z]+)', (req, res) => {
  res.sendFile(__dirname +  '/dist/index.html');
});

app.get('/ice_servers', (req, res) => {
  res.setHeader('Cache-Control', 'public, max-age=10800');
  if (new Date().getTime() - iceServersCache.updated < 60*60*1000 /* 1 hr cache */) {
    res.send(iceServersCache.value);
  } else {
    winston.debug('fetching new STUN/TURN info from twilio');
    twilio.tokens.create({}, function(err, token) {
      //mapping from url -> urls to match current standard
      var ice_servers=[];
      for(var i = 0; i < token.ice_servers.length; i++) {
        ice_servers[i] = {
          'urls': token.ice_servers[i].url,
          'username': token.ice_servers[i].username,
          'credential': token.ice_servers[i].credential
        };
      }
      iceServersCache = {
        updated: new Date().getTime(),
        value: ice_servers
      }
      res.send(ice_servers);
    });
  }
});

function resultOrFail(res, callback) {
  return (err, result) => {
    if (err) {
      console.warn(err);
      res.status(500).end();
    } else {
      callback(result);
    }
  }
}

io.on('connection', (socket) => {
  socket.on('disconnect', async (reason) => {
    const socketInfo = await redis.get(`socket:${socket.id}`);
    if(socketInfo) {
      const [room, frand] = socketInfo.split('#');
      socket.to(`room:${room}`).emit('dconn', frand);

      if (frand == 2) {
        winston.info(`[${chalk.gray(room)}] ${chalk.red('-')} p${frand} disconnect (forget in 30 seconds)`);
        redis.expire(`room:${room}:p2`, 30); // let another p2 join if no reconnect in 30 seconds
      } else {
        winston.info(`[${chalk.gray(room)}] ${chalk.red('-')} p${frand} disconnect`);
      }

      redis.del(`socket:${socket.id}`);
      redis.lrem('roulette_rooms:open', 0, room);
    }
  });

  socket.on('move', (moves, room, player) => {
    socket.to(`room:${room}`).emit('move', moves, player);
  });

  socket.on('message', function (message, room) {
    socket.to(`room:${room}`).emit('message', message);
  });

  socket.on('hug success', async (blob) => {
    const already_hugged = await redis.getset(`room:${blob.room}:hugsuccess`, "1");
    if (already_hugged !== "1") {
      winston.info(`[${chalk.gray(blob.room)}] ${chalk.green('$')} HUG SUCCESS!!`);
      await redis.incr("wins:hugs");
    }
    const stats = await redis.mget('wins:hugs', 'wins:kishks', 'wins:jumps', 'wins:hugzone');
    io.emit("stats", stats);
  });

  socket.on('jump success', async (blob) => {
    const already_jumped = await redis.getset(`room:${blob.room}:jumpsuccess`, "1");
    if (already_jumped !== "1") {
      winston.info(`[${chalk.gray(blob.room)}] ${chalk.green('$')} JUMP SUCCESS!!!`);
      await redis.incr("wins:jumps");
    }
    const stats = await redis.mget('wins:hugs', 'wins:kishks', 'wins:jumps', 'wins:hugzone');
    io.emit("stats", stats);
  });

  socket.on('kishk success', async (blob) => {
    const already_kishked = await redis.getset(`room:${blob.room}:kishksuccess`, "1");
    if (already_kishked !== "1") {
      winston.info(`[${chalk.gray(blob.room)}] ${chalk.green('$')} KISHK SUCCESS!!!!!!`);
      await redis.incr("wins:kishks");
    }
    const stats = await redis.mget('wins:hugs', 'wins:kishks', 'wins:jumps', 'wins:hugzone');
    io.emit("stats", stats);
  });

  socket.on('hugzone success', async (blob) => {
    const already_zoned = await redis.getset(`room:${blob.room}:zonesuccess`, "1");
    if (already_zoned !== "1") {
      winston.info(`[${chalk.gray(blob.room)}] ${chalk.green('$')} HUGZONE SUCCESS!!!!!!`);
      await redis.incr("wins:hugzone");
    }
    const stats = await redis.mget('wins:hugs', 'wins:kishks', 'wins:jumps', 'wins:hugzone');
    io.emit("stats", stats);
  });

  socket.on('need stats', async () => {
    const stats = await redis.mget('wins:hugs', 'wins:kishks', 'wins:jumps', 'wins:hugzone');

    socket.emit("stats", stats);
  });

  socket.on('join room', async (blob) => {
    const { room, frand_id: frand, prompt_first } = blob;
    const chan = io.in(`room:${room}`);

    const [frand1, frand2, is_roulette] = await redis.mget(
      `room:${room}:p1`,
      `room:${room}:p2`,
      `room:${room}:roulette`);

    if (!frand1) { // p1 enter (new room)
      winston.info(`[${chalk.gray(room)}] ${chalk.green('+')} p1 enter (new)`);

      redis.multi()
        .set(`room:${room}:p1`, frand, 'ex', 2592000)
        .set(`socket:${socket.id}`, `${room}#1`, 'ex', 2592000)
        .exec();

      socket.join(`room:${room}`);
      chan.emit('created', frand, !!is_roulette);
      if(!!is_roulette) {
        // add to list of roulette things
        redis.rpush('roulette_rooms:open', room);
      }
    } else if (frand1 === frand) { // p1 reconnect
      winston.info(`[${chalk.gray(room)}] ${chalk.blue('-')} p1 reconnect`);

      redis.set(`socket:${socket.id}`, `${room}#1`, 'ex', 2592000);

      socket.join(`room:${room}`);
      if (!!frand2) { //frand2 has joined
        chan.emit('rejoin', 1, frand);
      } else {
        chan.emit('created', frand, !!is_roulette);
      }
    } else if (frand2 === frand) { // p2 reconnect
      winston.info(`[${chalk.gray(room)}] ${chalk.blue('-')} p2 reconnect`);

      redis.multi()
        .set(`socket:${socket.id}`, `${room}#2`, 'ex', 2592000)
        .expire(`room:${room}:p2`, 2592000) // renew "lease" on p2 slot
        .exec();

      socket.join(`room:${room}`);
      chan.emit('rejoin', 2, frand);
    } else if (!frand2) { // p2 enter
      if (prompt_first && !is_roulette) {
        winston.info(`[${chalk.gray(room)}] ${chalk.green('+')} p2 thinkin about it`);
        socket.emit('joinable', frand);
      } else {
        winston.info(`[${chalk.gray(room)}] ${chalk.green('+')} p2 enter`);
        redis.multi()
          .set(`room:${room}:p2`, frand, 'ex', 2592000)
          .set(`socket:${socket.id}`, `${room}#2`, 'ex', 2592000)
          .exec();

        socket.join(`room:${room}`);
        chan.emit('joined', frand);
      }
    } else { // full room
      winston.warn(`[${chalk.gray(room)}] ${chalk.yellow('room full')}`);

      socket.emit('full', frand);
    }
  });
});

setInterval(() => {
  winston.info(`open websockets: ${io.engine.clientsCount}`);
}, 30000);

http.listen(3000, 'localhost', function() {
  winston.info('listening on %s:%s', http.address().address, http.address().port);
});
