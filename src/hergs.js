import "babel-polyfill";
require("webrtc-adapter/out/adapter_no_edge.js");
var Clipboard = require('clipboard');

import { logError, getSecureHexString, logPeerInfo } from './util';
import { keybodo, LEFT, UP, RIGHT, DOWN, SPACE } from './keybodo';

import * as zepto from './zepto';
var $ = window.$; // hack to get around zepto not being es6-friendly

/***********************************
 *                                 *
 *          MOBILE SECTION         *
 *                                 *
 ***********************************/


window.mobileAndTabletcheck = function() {
  var check = false;
  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
  return check;
};
$(".email-butt").on("click", e => {
  if($(".email-butt a").text() === "say hi to us") {
    e.preventDefault();
    $(".email-butt a").text("hold on a sec...");
    fetch('/contact_erdress').then(function(response) {
      response.blob().then(function(body) {
        $(".email-butt a").attr("href", "mailto:hergs@kermpany.com");
        $(".email-butt a").text("hergs@kermpany.com");
      });
    });
  }
});

$(document).ready(function() {
  if(window.mobileAndTabletcheck()) {
    $(".button-box").css("display", "flex");
    $(".move-button").on("mousedown", keybodo.onMouseDown);
    $(".move-button").on("mouseup", keybodo.onMouseUp);
    $(".move-button").on("touchstart", keybodo.onMouseDown);
    $(".move-button").on("touchend", keybodo.onMouseUp);
  }
});

/***********************************
 *                                 *
 *           GAME SECTION          *
 *                                 *
 ***********************************/

console.time("bootstrap");

var TIME_INTERVAL = 50;
var TIME_ELAPSE = 100;
var wins = {};
var gameBlob = {
  state: {
    timeElapsed: 0,
    guid: window.location.pathname.slice(1, window.location.pathname.length),
    frand_id: '',
    firstCollision: true,
    positions: {
      '1': {x: 0, arm: 0, frame: 2, heart: 0, lips: false, jump: false, hasSeenHeart: false},
      '2': {x: 700, arm: 0, frame: 2, heart: 0, lips: false, jump: false, hasSeenHeart: false},
      'orb': 0
    },
    lastPositions: {
      '1': {x: 0, arm: 0, frame: 2, heart: 0, lips: false, jump: false, hasSeenHeart: false},
      '2': {x: 700, arm: 0, frame: 2, heart: 0, lips: false, jump: false, hasSeenHeart: false},
      'orb': 0
    }
  },
  isWebRtc: undefined,
  SPEED: 6,
  updateWebRtcStatus () {
    var isWebRtc = !!(webrtcBlob.webrtcAvailable
      && webrtcBlob.frandrtcAvailable
      && webrtcBlob.dataChannel
      && webrtcBlob.dataChannel.readyState === 'open');

    if (gameBlob.isWebRtc !== isWebRtc) {
      console.info(`WebRTC status changed to ${(isWebRtc ? "available" : "unavailable")}.`);
      if (isWebRtc) logPeerInfo(webrtcBlob.peerConn);
    }
    gameBlob.isWebRtc = isWebRtc;
  },
  growHeart () {
    var heart = gameBlob.state.positions[gameBlob.state.player].heart;
    gameBlob.state.positions[gameBlob.state.player].heart = heart >= .8 ? .75 : heart + .05;

    gameBlob.state.positions[gameBlob.state.player].hasSeenHeart = true;
    if(heart > .7 && gameBlob.state.positions[gameBlob.state.otherPlayer].heart > .7) {
      gameBlob.growOrb();
    }
  },
  shrinkHeart () {
    var heart = gameBlob.state.positions[gameBlob.state.player].heart;
    gameBlob.state.positions[gameBlob.state.player].heart = heart - .10 < 0 ? 0 : heart - .10;
  },
  growOrb () {
    var orb = gameBlob.state.positions.orb;
    gameBlob.state.positions.orb = orb >= 1 ? 1 : orb + .02;
    if (wins['hug success'] !== true) {
      console.log("congratulations on your hug!!");
      socket.emit('hug success', { room: gameBlob.state.guid });
      wins['hug success'] = true;
    }
  },
  shrinkOrb () {
    var orb = gameBlob.state.positions.orb;
    gameBlob.state.positions.orb = orb - .1 <= 0 ? 0 : orb - .1;
  },
  movementAllowed (direction) {
    //232 being buffer for difference in drawing vs box size
    if(gameBlob.state.player == 1) {
      var isStuck = (gameBlob.state.positions[2].x - 142 <=  gameBlob.state.positions[1].x) && direction == -1;
      var hasRoom = gameBlob.state.positions[2].x - 142 >=  gameBlob.state.positions[1].x + (direction * gameBlob.SPEED);
      return isStuck || hasRoom;
    }
    else {
      var isStuck = (gameBlob.state.positions[2].x - 142 <=  gameBlob.state.positions[1].x) && direction == 1;
      var hasRoom = gameBlob.state.positions[2].x - 142 + (direction * gameBlob.SPEED) >=  gameBlob.state.positions[1].x;
      return isStuck || hasRoom;
    }
  },
  getStandFrame () {
    var positionDiff = gameBlob.state.positions['2'].x - gameBlob.state.positions['1'].x;
    return 2;
  },
  checkCollision () {
    var collision = gameBlob.state.positions['2'].x - gameBlob.state.positions['1'].x <= 144;
    if(collision && gameBlob.state.firstCollision) {
      $key_hints.hide();
      if(!gameBlob.state.positions[gameBlob.state.player].hasSeenHeart) {
        $space_hints.show();
      }
      gameBlob.state.firstCollision = false;
    }
    return collision;
  },
  checkKishkes () {
    var frandDifference = gameBlob.state.positions['2'].x - gameBlob.state.positions['1'].x;
    var kishk =  frandDifference <= 160 && gameBlob.state.positions['2'].x - gameBlob.state.positions['1'].x >= 145;
    return kishk;
  },
  animationLoop () {
    gameBlob.state.timeElapsed += TIME_INTERVAL;
    var inHugZone = gameBlob.checkCollision();
    var inKishkZone = gameBlob.checkKishkes();
    var transitionToStanding = false;
    //Moving character left and right
    //processMoves(); this is only if you want random movement
    if(keybodo.isDown([RIGHT, 'd']) && gameBlob.movementAllowed(1)) {
      if($key_hints.hasClass("key-hint-walk")) {
        $key_hints.removeClass("key-hint-walk");
        $key_hints.addClass("key-hint-arm");
      }
      gameBlob.state.positions[gameBlob.state.player].x += gameBlob.SPEED;
      if(gameBlob.state.timeElapsed  % TIME_ELAPSE == 0) {
        var newFrame = gameBlob.state.positions[gameBlob.state.player].frame + 1;
        gameBlob.state.positions[gameBlob.state.player].frame = (newFrame % 8) + 4;
      }
    }
    else if(keybodo.isDown([LEFT, 'a']) && gameBlob.movementAllowed(-1)) {
      if($key_hints.hasClass("key-hint-walk")) {
        $key_hints.removeClass("key-hint-walk");
        $key_hints.addClass("key-hint-arm");
      }
      gameBlob.state.positions[gameBlob.state.player].x -= gameBlob.SPEED;
      if(gameBlob.state.timeElapsed % TIME_ELAPSE == 0) {
        var newFrame = gameBlob.state.positions[gameBlob.state.player].frame + 1;
        gameBlob.state.positions[gameBlob.state.player].frame = (newFrame % 8) + 4;
      }
    }
    else {
      var currentFrame = gameBlob.state.positions[gameBlob.state.player].frame;
      var nextFrame = gameBlob.getStandFrame();
      transitionToStanding = currentFrame != nextFrame;
      gameBlob.state.positions[gameBlob.state.player].frame = nextFrame;
    }

    // swinging arm
    if(keybodo.isDown([UP, 'w'])) {
      setTimeout(() => $key_hints.hide(), 200);
      gameBlob.state.positions[gameBlob.state.player].arm -= 3;
    }
    else if(keybodo.isDown([DOWN, 's'])) {
      $key_hints.hide();
      gameBlob.state.positions[gameBlob.state.player].arm += 3;
    }
    //heart
    if(inHugZone && keybodo.isDown(SPACE)) {
      $("#space-hints").hide();
      gameBlob.growHeart();
    }
    else if (!keybodo.isDown(SPACE)) {
      gameBlob.shrinkHeart();
    }

    if(!inHugZone) {
      $("#space-hints").hide();
    }

    //hugzone
    if (inHugZone && wins['hugzone success'] !== true) {
      console.log("congratulations on your chest bump!!");
      socket.emit('hugzone success', { room: gameBlob.state.guid });
      wins['hugzone success'] = true;
    }

    //kishk
    const kishk = (inKishkZone && !!keybodo.isDown('k'));
    gameBlob.state.positions[gameBlob.state.player].lips = kishk;
    if (kishk && wins['kishk success'] !== true) {
      console.log("congratulations on your kishk!!");
      socket.emit('kishk success', { room: gameBlob.state.guid });
      wins['kishk success'] = true;
    }

    if (gameBlob.state.positions['1'].heart <= .7 || gameBlob.state.positions['2'].heart <= .7) {
      gameBlob.shrinkOrb();
    }

    //jump
    gameBlob.state.positions[gameBlob.state.player].jump = !!keybodo.isDown('j');
    if (!!keybodo.isDown('j') && wins['jump success'] !== true) {
      console.log("congratulations on your jump!!");
      socket.emit('jump success', { room: gameBlob.state.guid });
      wins['jump success'] = true;
    }

    var changed = JSON.stringify(gameBlob.state.lastPositions) != JSON.stringify(gameBlob.state.positions);

    //five
    /* if(calculateFiveSpot() && changed) {
      $(".five").css("left", gameBlob.state.positions[1].x + 75);
      $(".five").show();
      setTimeout(function() {
        $(".five").hide()
      }, 200);
    }*/

    var held = gameBlob.state.positions[gameBlob.state.player].jump || gameBlob.state.positions[gameBlob.state.player].kishk;
    gameBlob.updateWebRtcStatus();
    if (changed || held || transitionToStanding) {
      if(gameBlob.isWebRtc) {
        let blob = {
          newPos: gameBlob.state.positions[gameBlob.state.player],
          player: gameBlob.state.player
        };
        webrtcBlob.dataChannel.send(JSON.stringify(blob));
      } else {
        socket.emit('move',
          JSON.stringify(gameBlob.state.positions[gameBlob.state.player]),
          gameBlob.state.guid,
          gameBlob.state.player);
      }
      updatePositions();
    }
  }
};

window.gameBlob = gameBlob;

function handleMoves(e) {
  var data = JSON.parse(e.data);
  gameBlob.state.positions[data.player] = data.newPos;
  updatePositions();
}

/***********************************
 *                                 *
 *            UI SECTION           *
 *                                 *
 ***********************************/

let $linkity = $("#linkity"),
    $linkity_loading = $("#linkity_loading"),
    $linkity_looking = $("#linkity_looking"),
    $linkity_content = $("#linkity_content"),
    $linkity_container = $("#linkity_container"),
    $linkity_accept = $("#linkity_accept"),
    $linkity_link = $("#linkity_link");
let $p1 = $("#p1"),
    $p1_arm = $("#p1 .hug-arm"),
    $p1_body = $("#p1 .hug-body"),
    $p1_heart = $("#p1 .heart"),
    $p1_mini_body = $(".bubb1 .mini-dude .hug-body"),
    $p1_mini_arm = $(".bubb1 .mini-dude .hug-arm");
let $p2 = $("#p2"),
    $p2_arm = $("#p2 .hug-arm"),
    $p2_body = $("#p2 .hug-body"),
    $p2_heart = $("#p2 .heart"),
    $p2_ghost = $("#p2-ghost"),
    $p2_mini_body = $(".bubb2 .mini-dude .hug-body"),
    $p2_mini_arm = $(".bubb2 .mini-dude .hug-arm");
let $key_hints = $("#key-hints");
let $space_hints = $("#space-hints");
let $connection_warning = $(".connection-warning");
let $hug_box = $(".hug-box");
let $orb_box = $(".orb-box");
let $hug_orb = $(".hug-orb");
let $dist_bubb_p1 = $("#bubb1");
let $dist_bubb_p2 = $("#bubb2");
let $dist_counter_p1 = $("#bubb1 .counter");
let $dist_counter_p2 = $("#bubb2 .counter");

function showLinkBox() {
  $linkity_loading.hide();
  $linkity_content.show();
  $linkity_content.addClass('fade-in');
  $linkity_container.css('width', $('#linkity_content').width());
  $(document).on('click','input[type=text]', () => { this.select(); });
  $linkity_link.val(window.location.href);
}

function showAcceptBox() {
  $linkity_loading.hide();
  $linkity_accept.show();
  $linkity_accept.addClass('fade-in');
  $linkity_container.css('width', $linkity_accept.width());
}

function showFrand() {
  $connection_warning.hide();
  $linkity.hide();
  $p2_ghost.hide();
  $p2.show();
  $hug_box.css('opacity', '1');

  const { positions } = gameBlob.state;
  if(gameBlob.state.player == 1) {
    $key_hints.css("left", (positions['1'].x + 64));
  }
  else {
    $key_hints.css("left", (positions['2'].x - 220));
    $key_hints.css("transform", "scale(-1, 1)");
  }
  $key_hints.show();
}

var updatePositions = function() {
  var armTops = [0, 0, 0, 1, 14, 1, -5, 1, 14, 1, -5, 1];

  var positions = gameBlob.state.positions;

  $p1.css("left", positions['1'].x + "px");
  $p1_arm.css("transform", "rotate(" + positions['1'].arm + "deg)");
  $p1_arm.css("top", armTops[positions['1'].frame]);
  $p1_body.css("background-position-x", positions['1'].frame * -105 + "px");
  $p1_heart.css("transform", "scale("+positions['1'].heart+")");

  $p1_mini_arm.css("transform", "rotate(" + positions['1'].arm + "deg)");
  $p1_mini_arm.css("top", armTops[positions['1'].frame]);
  $p1_mini_body.css("background-position-x", positions['1'].frame * -105 + "px");

  $p2.css("left", positions['2'].x + "px");
  $p2_arm.css("transform", "rotate(" + positions['2'].arm + "deg)");
  $p2_arm.css("top", armTops[positions['2'].frame])
  $p2_body.css("background-position-x", positions['2'].frame * -105 + "px");
  $p2_heart.css("transform", "scale("+positions['2'].heart+")");

  $p2_mini_arm.css("transform", "rotate(" + positions['2'].arm + "deg)");
  $p2_mini_arm.css("top", armTops[positions['2'].frame]);
  $p2_mini_body.css("background-position-x", positions['2'].frame * -105 + "px");

  if(gameBlob.state.player == 1) {
    $space_hints.css("transform", "scale(-1, 1)");
    $space_hints.css("left", positions['1'].x - 120);
    $key_hints.css("left", positions['1'].x + 64);
  }
  else {
    $key_hints.css("left", positions['2'].x - 220);
    $space_hints.css("left", positions['2'].x - 25);
  }
  $orb_box.css("left", (positions['1'].x - 329) + "px");
  $hug_orb.css("transform", "scale("+positions.orb+")");

  if(gameBlob.state.positions[gameBlob.state.player].lips) {
    let $el = gameBlob.state.player == 1 ? $p1_body : $p2_body;
    $el.css("background-position-x", "-3145px");
  }

  if(gameBlob.state.positions[gameBlob.state.otherPlayer].lips) {
    let $el = gameBlob.state.otherPlayer == 1 ? $p1_body : $p2_body;
    $el.css("background-position-x", "-3145px");
  }

  if(gameBlob.state.positions[gameBlob.state.player].jump) {
    let $body = gameBlob.state.player == 1 ? $p1_body : $p2_body;
    let $arm = gameBlob.state.player == 1 ? $p1_arm : $p2_arm;
    let $mini_body = gameBlob.state.player == 1 ? $p1_mini_body : $p2_mini_body;
    let $mini_arm = gameBlob.state.player == 1 ? $p1_mini_arm : $p2_mini_arm;

    $body.addClass("jumpin");
    $arm.addClass("jump-arm");
    $mini_body.addClass("jumpin");
    $mini_arm.addClass("jump-arm");
  }
  if(gameBlob.state.positions[gameBlob.state.otherPlayer].jump) {
    let $body = gameBlob.state.otherPlayer == 1 ? $p1_body : $p2_body;
    let $arm = gameBlob.state.otherPlayer == 1 ? $p1_arm : $p2_arm;
    let $mini_body = gameBlob.state.otherPlayer == 1 ? $p1_mini_body : $p2_mini_body;
    let $mini_arm = gameBlob.state.otherPlayer == 1 ? $p1_mini_arm : $p2_mini_arm;

    $body.addClass("jumpin");
    $arm.addClass("jump-arm");
    $mini_body.addClass("jumpin");
    $mini_arm.addClass("jump-arm");
  }

  gameBlob.state.lastPositions = JSON.parse(JSON.stringify(positions));
  localStorage.setItem("positions", JSON.stringify(positions));

  updateDistanceCounter();
}

function updateDistanceCounter() {
  const PIXELS_PER_SHMEETER = 25;
  var distanceLeft = (window.innerWidth - 700) / 2 + (105 * 2 / 3);
  var distanceRight = (window.innerWidth - 700) / 2 + (105 * 2 / 3);

  var distanceOffScreenLeft = (distanceLeft + gameBlob.state.positions[1].x + 54);
  var distanceOffScreenRight = (distanceRight - gameBlob.state.positions[2].x + 700);

  distanceOffScreenLeft = distanceOffScreenLeft / PIXELS_PER_SHMEETER;
  distanceOffScreenRight = distanceOffScreenRight / PIXELS_PER_SHMEETER;

  if (distanceOffScreenLeft <= 0) {
    $dist_bubb_p1.show();
    $dist_counter_p1.text(`${Math.ceil(-distanceOffScreenLeft)}ft`);
  } else {
    $dist_bubb_p1.hide();
  }

  if (distanceOffScreenRight <= 0) {
    $dist_bubb_p2.show();
    $dist_counter_p2.text(`${Math.ceil(-distanceOffScreenRight)}ft`);
  } else {
    $dist_bubb_p2.hide();
  }


}

function calculateFiveSpot() {
  // if the hands are angled properly see if they are close enough
  var p1Hands = calculateHandPostion(1);
  var p2Hands = calculateHandPostion(2);
  if(p1Hands.x == p2Hands.x && p1Hands.y == p2Hands.y) {
    var diff = gameBlob.state.positions[2].x - gameBlob.state.positions[1].x;
    return (diff > 60 && diff <= 200);
  }
}

function calculateHandPostion(player) {
  var armLength = 46.7;
  var deg = gameBlob.state.positions[player].arm;

  var initX = armLength;
  var initY = 0;

  var angleOne = (Math.PI/180)*deg;
  var angleTwo = (Math.PI/180)*(90-deg);

  var sideOne = armLength * Math.sin(angleOne);
  var sideTwo = armLength * Math.sin(angleTwo);

  var changeX = armLength - sideTwo;
  var changeY = sideOne;

  //console.log(initX-changeX,initY+changeY)
  return {x: initX-changeX, y: initY+changeY};
}

function checkForFive() {

}

/***********************************
 *                                 *
 *          WEBRTC SECTION         *
 *                                 *
 ***********************************/

var webrtcBlob = {
  peerConn: null,
  dataChannel: null,
  isChannelReady: false,
  isInitiator: false,
  isStarted: false,
  config: {
   'iceServers': [
      { urls: 'stun:stun.l.google.com:19302' },
      { urls: 'stun:stun.services.mozilla.com:3478' },
      { urls: 'stun:stun1.l.google.com:19302' },
      { urls: 'stun:stun2.l.google.com:19302' },
      { urls: 'stun:stun3.l.google.com:19302' },
      { urls: 'stun:stun4.l.google.com:19302' },
    ]
  }
};

fetch("/ice_servers", {credentials: 'same-origin'})
  .then(response => {
    if (!response.ok) return new Error(response);
    return response.json();
  })
  .then((ice_servers) => {
    webrtcBlob.config['iceServers'] = ice_servers;
    setupSocket();
  })
  .catch((err) => {
    console.warn("failed to fetch ice servers");
    console.warn(err);
    setupSocket();
  });


function checkWebRTCAvailability() {
  var frandable = !!window.mozRTCPeerConnection || !!window.RTCPeerConnection || !!window.webkitRTCPeerConnection;
  sendMessage({type: 'frandibility', value: frandable}, gameBlob.state.guid);
  return frandable;
}

function createPeerConnection() {
  webrtcBlob.peerConn = new RTCPeerConnection(webrtcBlob.config);

  if(webrtcBlob.isInitiator) {
    webrtcBlob.dataChannel = webrtcBlob.peerConn.createDataChannel('moves');
    onDataChannelCreated(webrtcBlob.dataChannel);

    //console.log("creating offer");
    webrtcBlob.peerConn.createOffer().then(onLocalSessionCreated).catch(logError);
  }
  else {
    webrtcBlob.peerConn.ondatachannel = function(e) {
      //console.log("ondatachannel(%s)", e.channel);
      webrtcBlob.dataChannel = e.channel;
      onDataChannelCreated(webrtcBlob.dataChannel);
    }
  }

  webrtcBlob.peerConn.onicecandidate = function(e) {
    if(e.candidate) {
      sendMessage({
        type: 'candidate',
        label: e.candidate.sdpMLineIndex,
        id: e.candidate.sdpMid,
        candidate: e.candidate.candidate,
      }, gameBlob.state.guid);
    }
  }
}

function onDataChannelCreated(channel) {
  channel.onmessage = handleMoves;
  gameBlob.animationInterval = gameBlob.animationInterval || setInterval(gameBlob.animationLoop, TIME_INTERVAL);
}


function onLocalSessionCreated(desc) {
  webrtcBlob.peerConn.setLocalDescription(desc).then(function() {
    console.debug('sending local description', webrtcBlob.peerConn.localDescription);
    sendMessage(webrtcBlob.peerConn.localDescription, gameBlob.state.guid);
  }).catch(logError);
}

function handleSignalingMessage(message) {
  console.debug("handleSignal(%s) [state: %s]", message.type, webrtcBlob.peerConn.signalingState);
  switch(message.type) {
    case 'offer':
      checkWebRTCAvailability();
      webrtcBlob.peerConn.setRemoteDescription(new RTCSessionDescription(message))
        .then(function() {
          webrtcBlob.peerConn.createAnswer()
            .then(onLocalSessionCreated)
            .catch(logError);
        })
        .catch(logError);
      break;
    case 'answer':
      webrtcBlob.peerConn.setRemoteDescription(new RTCSessionDescription(message))
        .catch(logError);
      break;
    case 'candidate':
      webrtcBlob.peerConn.addIceCandidate(new RTCIceCandidate({candidate: message.candidate}));
      break;
    case 'frandibility':
      webrtcBlob.frandrtcAvailable = message.value;
      break;
    case 'ayyyyy':
      createPeerConnection();
      checkWebRTCAvailability();
      break;
  };
}

/***********************************
 *                                 *
 *          SOCKET SECTION         *
 *                                 *
 ***********************************/

var socket = null;

function setupSocket() {
  console.timeEnd("bootstrap");
  console.time("create or join room");
  socket = io.connect();
  gameBlob.state.frand_id = getSecureHexString(gameBlob);
  socket.emit('join room', {
    room: gameBlob.state.guid,
    frand_id: gameBlob.state.frand_id,
    prompt_first: true,
  });

  $("#yep").on('click', (e) => {
    socket.emit('join room', {
      room: gameBlob.state.guid,
      frand_id: gameBlob.state.frand_id,
      prompt_first: false
    });
  });

  $("#nope").on('click', (e) => {
    $('#why-nope').show();
    $linkity_container.css('height', $linkity_accept.height());
  });

  socket.on('created', (frand, isRoulette) => {
    console.timeEnd("create or join room");
    if(frand === gameBlob.state.frand_id) {
      webrtcBlob.isInitiator = true;
      gameBlob.state.player = 1;
      gameBlob.state.otherPlayer = 2;
      gameBlob.state.isRoulette = isRoulette;
      if(!isRoulette) {
        showLinkBox();
      }
      else {
        $linkity_loading.hide();
        $linkity_looking.show();
      }
    }
  });

  socket.on('joinable', (frand) => {
    console.timeEnd("create or join room");
    showAcceptBox();
  });

  socket.on('joined', (frand) => {
    console.timeEnd("create or join room");
    if(frand === gameBlob.state.frand_id) {
      webrtcBlob.isInitiator = false;
      gameBlob.state.player = 2;
      gameBlob.state.otherPlayer = 1;
    }
    readyOrRejoin();
  });

  socket.on('full', (frand) => {
    $("#room_full").show();
  });

  socket.on('rejoin', (player, frand) => {
    console.timeEnd("create or join room");
    $(".connection-warning").hide();
    $(".ghost-man").hide();
    $(".solid-man").show();

    if(frand === gameBlob.state.frand_id) {
      gameBlob.state.player      = player;
      gameBlob.state.otherPlayer = player === 1 ? 2 : 1;
      webrtcBlob.isInitiator     = player === 1 ? true : false;

      sendMessage({type: 'ayyyyy'}, gameBlob.state.guid);

      if (localStorage.getItem("positions") !== null) {
        gameBlob.state.positions = JSON.parse(localStorage.getItem("positions"));
      }

      updatePositions();
      readyOrRejoin();
    }
  });

  socket.on('dconn', (frand) => {
    if(frand !== gameBlob.state.frand_id) {
      console.warn(`connection lost, frand ${frand}`);
      $("#p"+frand).hide();
      $("#p"+frand+"-ghost").css("left", gameBlob.state.positions[frand].x);
      $("#p"+frand+"-ghost").show();
      $(".connection-warning").show();
    } else {
      console.warn(`got disconnect event for self...`);
    }
  });

  socket.on('move', (move, player) => {
    gameBlob.state.positions[player] = JSON.parse(move);
    updatePositions();
  });

  socket.on('message', handleSignalingMessage);

  socket.on('log', (array) => {
    console.debug.apply(console, array);
  });
}

function readyOrRejoin() {
  webrtcBlob.isChannelReady = true;
  showFrand();

  var audio = new Audio('audio/hugtime.m4a');
  audio.play();

  // Check to see if we need to fallback to sockets
  webrtcBlob.webrtcAvailable = checkWebRTCAvailability();

  // TODO make sure that the 2 Available values are set before running this...
  // Only create peer connection if both clients are able
  gameBlob.animationInterval = gameBlob.animationInterval || setInterval(gameBlob.animationLoop, TIME_INTERVAL);
  if(webrtcBlob.webrtcAvailable /*&& webrtcBlob.frandrtcAvailable*/) {
    createPeerConnection();
  }
}

function sendMessage(message, room) {
  socket.emit('message', message, room);
}

$(window).on('keydown', keybodo.onButtonDown);
$(window).on('keyup', keybodo.onButtonUp);
$(window).on('blur', keybodo.clearAll);

$(".hug-body")
  .on("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd",
      (e) => { $(e.currentTarget).removeClass('jumpin'); });

$(".hug-arm")
  .on("animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd",
      (e) => { $(e.currentTarget).removeClass('jump-arm'); });

var clippy = new Clipboard('.btn');


/***********************************
 *                                 *
 *       MOVE BY JSON SECTION      *
 *                                 *
 ***********************************/

 var movesBlob = {
  200: {
    button: RIGHT,
    duration: 2000
  },
  2050: {
    button: LEFT,
    duration: 800
  },
  2050: {
    button: UP,
    duration: 800
  },
};
var timeouts = {};
function processMoves() {
  var move = generateMoves();

  if (move) {
    keybodo.keysDown[move.button] = true;
    if(timeouts[move.button]) {
      clearTimeout(timeouts[move.button]);
    }
    timeouts[move.button] = setTimeout(function() { delete keybodo.keysDown[move.button]; }, move.duration);
  }
}

function generateMoves() {
  // left / right most common (favor towards other player)
  // arm movement / hugging second most common
  // jump / kiss not so common
  var possibleKeys = {
    '1': [LEFT, RIGHT, UP, DOWN, SPACE, 74, 75],
    '2': [LEFT, RIGHT, UP, DOWN, SPACE, 74, 75]
  };

  var random256 = new Uint8Array(1);
  var random65536 = new Uint16Array(1);
  var bool = Math.floor(window.crypto.getRandomValues(random256)[0]/232);

  if(bool) {
    var keyIndex = Math.floor(window.crypto.getRandomValues(random256)[0]/(256/7));
    var duration = Math.floor(window.crypto.getRandomValues(random65536)[0]/100);
  }

  return { button: possibleKeys[gameBlob.state.player][keyIndex], duration: duration };
}
