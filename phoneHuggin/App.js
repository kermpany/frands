import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
} from 'react-native';

import io from 'socket.io-client';

const { width, height } = Dimensions.get('window');
const SHRINK = .54;
const TIME_ELAPSE = 100;
const TIME_INTERVAL = 50;

export default class App extends React.Component {
  constructor(props) {

    super(props);
    this.fetchGuid();
    this.state = {
      SPEED: 6,
      timeElapsed: 0,
      guid: null,
      frand_id: '',
      firstCollision: true,
      orbFrame: 0,
      positions: {
        '1': {x: 0, arm: 0, frame: 2, heart: 0, lips: false, jump: false, hasSeenHeart: false},
        '2': {x: 700, arm: 0, frame: 2, heart: 0, lips: false, jump: false, hasSeenHeart: false},
        'orb': 0
      },
      lastPositions: {
        '1': {x: 0, arm: 0, frame: 2, heart: 0, lips: false, jump: false, hasSeenHeart: false},
        '2': {x: 700, arm: 0, frame: 2, heart: 0, lips: false, jump: false, hasSeenHeart: false},
        'orb': 0
      },
      actions: {
        'right': false,
        'up': false,
        'down': false,
        'left': false,
        'hug': false,
        'kishk': false,
        'jump': false,
      }
    };
    this.socket = null;
    this.armTops = [0, 0, 0, 1, 14, 1, -5, 1, 14, 1, -5, 1];
    console.log(width, height);
  }

  componentDidMount() {
    animationInterval = setInterval(() => this._animationLoop(), TIME_INTERVAL);
    this.setState({
      ...this.state,
      animationInterval,
    });
  }

  async fetchGuid() {
    const response = await fetch('https://frands.space/grab_guid');
    const guid = await response.text();
    this.setState({
      ...this.state,
      guid,
    }, this.setupSocket);
  }

  emitMoves = () => {
    this.socket.emit('move', JSON.stringify(this.state.positions['1']), this.state.guid, 1);
  }

  shrinkOrb  = () => {
    const { orb } = this.state.positions;
    return orb - .1 <= 0 ? 0 : orb - .1;
  }

  growOrb = () => {
    const { orb } = this.state.positions;
    return orb >= 1 ? 1 : orb + .02;
  }

  growHeart = () => {
    const { heart } = this.state.positions['1'];
    return heart >= .8 ? .75 : heart + .05;
  }

  shrinkHeart = () => {
    const { heart } = this.state.positions['1'];
    return ((heart - .10) < 0) ? 0 : heart - .10;
  }

  checkCollision = () => {
    // TODO: make sure this 38 makes sense for other screen sizes
    const collision = this.state.positions['2'].x - this.state.positions['1'].x <= 144;
    // if(this.state.timeElapsed % 300 == 0) {
    //   console.log(`${this.state.positions['2'].x} ${this.state.positions['1'].x}`);
    //   console.log(`${this.state.positions['2'].x - this.state.positions['1'].x} <= ${144*SHRINK}`);
    //   console.log(collision);
    // }
    //console.log(collision);
    return collision;
  }

  movementAllowed = (direction) => {
    const { positions, SPEED } = this.state;
    const isStuck = (positions['2'].x - 142 <= positions['1'].x) && direction == -1;
    const hasRoom = positions['2'].x - 142 >= (positions['1'].x + (direction * SPEED));
    //console.log(isStuck, hasRoom);
    return isStuck || hasRoom;
  }

  _animationLoop = () => {
    if (!this.socket) { return; }
    const { actions, timeElapsed } = this.state;
    const playerPositions = this.state.positions['1'];
    let newX = playerPositions.x;
    let newArm = playerPositions.arm;
    let newFrame = playerPositions.frame;
    let newHeart = playerPositions.heart;
    let newOrb = this.state.positions.orb;
    let newOrbFrame = this.state.orbFrame;

    const inHugZone = this.checkCollision();

    if(timeElapsed % TIME_ELAPSE === 0) {
      newFrame = newFrame + 1;
      newFrame = (newFrame % 8) + 4;
    }

    if (actions['right'] && this.movementAllowed(1)) { // move right
      newX = playerPositions.x + this.state.SPEED;
    } else if (actions['left'] && this.movementAllowed(-1)) { // move left
      newX = playerPositions.x - this.state.SPEED;
    } else { // standing frame
      newFrame = 2;
    }

    if (actions['up']) { // move arm up
      newArm = playerPositions.arm - 3;
    } else if (actions['down']) { // move arm down
      newArm = playerPositions.arm + 3;
    }

    if (inHugZone && actions['hug']) {
      newHeart = this.growHeart();
      newOrbFrame = (newOrbFrame + 1) % 15;
      if(newHeart > .7 && this.state.positions['2'].heart > .7) {
        newOrb = this.growOrb();
      } else {
        newOrb = this.shrinkOrb();
      }
    } else if(!actions['hug']) {
      newHeart = this.shrinkHeart();
      newOrb = this.shrinkOrb();
    }
    const player = '1';

    this.setState({
      ...this.state,
      positions: {
        ...this.state.positions,
        [player]: {
          ...this.state.positions[player],
          x: newX,
          frame: newFrame,
          arm: newArm,
          heart: newHeart,
        },
        orb: newOrb,
      },
      orbFrame: newOrbFrame,
      timeElapsed: timeElapsed + TIME_INTERVAL,
    }, this.emitMoves);
  }

  _setAction = (action, value) => {
    this.setState({
      ...this.state,
      actions: {
        ...this.state.actions,
        [action]: value,
      },
    });
  }

  render() {
    const { positions, guid } = this.state;
    const p1Offset = positions['1'].frame * 105 * SHRINK;
    const p2Offset = positions['2'].frame * 105 * SHRINK;
    const p1Margin = p1Offset - (positions['1'].x * SHRINK);
    const p2Margin = p2Offset - ((positions['2'].x - 105)* SHRINK);
    const p1ArmTop = this.armTops[positions['1'].frame] * SHRINK;
    const p2ArmTop = this.armTops[positions['2'].frame] * SHRINK;
    const cirqueOffset = (positions['1'].x - 280) * SHRINK;
    const armMarginLeft = 0 * SHRINK;
    const armMarginTop = 0 * SHRINK;
    console.log(cirqueOffset);
    return (
      <View style={styles.box}>
        <View style={styles.hugBox}>
          <View style={[styles.cirqueContainer, {left: cirqueOffset, top: -140}]}>
            <Image
              style={[styles.cirque, {transform: [{ scale: this.state.positions.orb }] }]}
              source={require('./love_cirque_sm.gif')}
            />
          </View>
          <View style={styles.flerrContainer}>
            <Image
              style={[styles.flerr]}
              source={require('./flerr.png')}
            />
          </View>
          <View style={[styles.frandBox, { left: p1Offset, top: 0, marginLeft: -p1Margin, marginTop: 0 }]}>
            <Image
              style={[styles.hugFrand, { marginLeft: -p1Offset, marginTop: 0 }]}
              source={require('./front-side-back-walk-turn-kiss-jump.png')}
            />
            <Image
              style={[styles.hugArm, { marginLeft: armMarginLeft, marginTop: armMarginTop + p1ArmTop, transform: [{ rotate: positions['1'].arm+'deg' }] }]}
              source={require('./arm2.png')}
            />
            <Image
              style={[styles.heart, { marginLeft: 0, marginTop: 0, transform: [{ scale: positions['1'].heart }] }]}
              source={require('./heart.png')}
            />
          </View>
          <View style={[styles.frandBox, { left: p2Offset, top: 0, marginLeft: -p2Margin, marginTop: 0, transform: [{scaleX: -1}] }]}>
            <Image
              style={[styles.hugFrand, { marginLeft: -p2Offset, marginTop: 0 }]}
              source={require('./front-side-back-walk-turn-kiss-jump.png')}
            />
            <Image
              style={[styles.hugArm, { marginLeft: armMarginLeft, marginTop: armMarginTop + p2ArmTop, transform: [{ rotate: positions['2'].arm +'deg' }] }]}
              source={require('./arm2.png')}
            />
            <Image
              style={[styles.heart, { marginLeft: 0, marginTop: 0, transform: [{ scale: positions['2'].heart }] }]}
              source={require('./heart.png')}
            />
          </View>
        </View>
        <View style={styles.buttonGroup}>
          <View style={styles.buttonsLeft}>
            <View style={styles.button} onTouchStart={() => this._setAction('up', true)} onTouchEnd={() => this._setAction('up', false)}>
              <Image
                style={[styles.buttonImage]}
                source={require('./up.png')}
              />
            </View>
            <View style={styles.button} onTouchStart={() => this._setAction('left', true)} onTouchEnd={() => this._setAction('left', false)}>
              <Image
                style={[styles.buttonImage]}
                source={require('./left.png')}
              />
            </View>
          </View>
          <View>
            <View style={styles.button} onTouchStart={() => this._setAction('hug', true)} onTouchEnd={() => this._setAction('hug', false)}>
              <Image
                style={[styles.buttonImage]}
                source={require('./space.png')}
              />
            </View>
          </View>
          <View style={styles.buttonsRight}>
            <View style={styles.button} onTouchStart={() => this._setAction('down', true)} onTouchEnd={() => this._setAction('down', false)}>
              <Image
                style={[styles.buttonImage]}
                source={require('./down.png')}
              />
            </View>
            <View style={styles.button} onTouchStart={() => this._setAction('right', true)} onTouchEnd={() => this._setAction('right', false)}>
              <Image
                style={[styles.buttonImage]}
                source={require('./right.png')}
              />
            </View>
          </View>
        </View>
        <Text style={{position: 'absolute', marginBottom: 20, bottom: 0}}>{guid}</Text>
      </View>
    );
  }

  setupSocket = () => {
    console.log("bootstrap");
    console.log("create or join room start");
    console.log(this.state);

    this.socket = io('https://frands.space', {
      transports: ['websocket'] // you need to explicitly tell it to use websockets
    });

    this.socket.on('connect', () => {
      console.log('connected!');
    });
    // gameBlob.state.frand_id = getSecureHexString(gameBlob);
    console.log(this.state.guid);
    this.socket.emit('join room', {
      room: this.state.guid,
      frand_id: 0,
      prompt_first: true,
    });

    this.socket.on('created', (frand, isRoulette) => {
      console.log("create or join room end");
      if(frand === 0) {
        console.log("frand0");
        //webrtcBlob.isInitiator = true;
        //gameBlob.state.player = 1;
        //gameBlob.state.otherPlayer = 2;
        //gameBlob.state.isRoulette = isRoulette;
      }
    });

    this.socket.on('joinable', (frand) => {
      console.log("create or join room");
      //showAcceptBox();
    });

    this.socket.on('joined', (frand) => {
      console.log("create or join room");
      if(frand === 0) {
        console.log("beeeep");
        // webrtcBlob.isInitiator = false;
        // gameBlob.state.player = 2;
        // gameBlob.state.otherPlayer = 1;
      }
      //readyOrRejoin();
    });

    this.socket.on('full', (frand) => {
      console.log("full");
    });

    this.socket.on('rejoin', (player, frand) => {
      console.log("create or join room rejoin");

      if(frand === 0) {
        console.log("reset");
        // gameBlob.state.player      = player;
        // gameBlob.state.otherPlayer = player === 1 ? 2 : 1;
        // webrtcBlob.isInitiator     = player === 1 ? true : false;

        // sendMessage({type: 'ayyyyy'}, gameBlob.state.guid);

        // if (localStorage.getItem("positions") !== null) {
        //   gameBlob.state.positions = JSON.parse(localStorage.getItem("positions"));
        // }

        // updatePositions();
        // readyOrRejoin();
      }
    });

    this.socket.on('dconn', (frand) => {
      if(frand !== gameBlob.state.frand_id) {
        console.warn(`connection lost, frand ${frand}`);
        // $("#p"+frand).hide();
        // $("#p"+frand+"-ghost").css("left", gameBlob.state.positions[frand].x);
        // $("#p"+frand+"-ghost").show();
        // $(".connection-warning").show();
      } else {
        console.warn(`got disconnect event for self...`);
      }
    });

    this.socket.on('move', (move, player) => {
      console.log("move", move);
      this.setState({
        ...this.state,
        positions: {
          ...this.state.positions,
          [player]: JSON.parse(move),
        }
      });
    });

    this.socket.on('message', () => console.log('message'));

    this.socket.on('log', (array) => {
      console.debug.apply(console, array);
    });
  }

}

const styles = StyleSheet.create({
  box: {
    flex: 1,
    alignItems: 'center',
  },
  frandBox: {
    width: 105 * SHRINK,
    height: 208 * SHRINK,
    overflow: 'hidden',
    position: 'absolute',
  },
  hugBox: {
    marginTop: 40,
    position: 'relative',
    width: 700 * SHRINK,
  },
  hugFrand: {
    width: 3255 * SHRINK,
    height: 416 * SHRINK,
    resizeMode: 'cover',
    position: 'absolute',
  },
  hugArm: {
    width: 100 * SHRINK,
    height: 156 * SHRINK,
    resizeMode: 'cover',
    position: 'absolute',
  },
  heart: {
    width: 44 * SHRINK,
    height: 37 * SHRINK,
    resizeMode: 'cover',
    position: 'absolute',
    top: 77 * SHRINK,
    left: 32 * SHRINK,
  },
  cirqueContainer: {
    width: 704 * SHRINK,
    height: 704 * SHRINK,
    overflow: 'hidden',
    position: 'absolute',
  },
  cirque: {
    width: 704 * SHRINK,
    height: 704 * SHRINK,
    resizeMode: 'contain',
    position: 'absolute',
    //top: 77 * SHRINK,
    //left: 32 * SHRINK,
  },
  flerrContainer: {
    alignItems: 'center',
    flex: 1,
    marginTop: 190 * SHRINK,
    //position: 'absolute',
  },
  flerr: {
    width: 700 * SHRINK,
    height: 20 * SHRINK,
    resizeMode: 'cover',
  },
  buttonsLeft: {
  },
  buttonsRight: {
  },
  buttonGroup: {
    width: '100%',
    position: 'absolute',
    bottom: 0,
    marginBottom: 20,
    paddingHorizontal: 10,
    //marginTop: 200,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button: {
    height: 40,
    marginTop: 6,
  },
  buttonImage: {
    resizeMode: 'contain',
    flex: 1,
  }
});
