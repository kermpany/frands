import io from 'socket.io-client';

export function setupSocket() {
  console.log("bootstrap");
  console.log("create or join room start");
  const socket = io('https://frands.space', {
    transports: ['websocket'] // you need to explicitly tell it to use websockets
  });

  socket.on('connect', () => {
    console.log('connected!');
  });
  // gameBlob.state.frand_id = getSecureHexString(gameBlob);
  socket.emit('join room', {
    room: 'phone',
    frand_id: 0,
    prompt_first: true,
  });

  socket.on('created', (frand, isRoulette) => {
    console.log("create or join room end");
    if(frand === 0) {
      console.log("frand0");
      //webrtcBlob.isInitiator = true;
      //gameBlob.state.player = 1;
      //gameBlob.state.otherPlayer = 2;
      //gameBlob.state.isRoulette = isRoulette;
    }
  });

  socket.on('joinable', (frand) => {
    console.log("create or join room");
    //showAcceptBox();
  });

  socket.on('joined', (frand) => {
    console.log("create or join room");
    if(frand === 0) {
      console.log("beeeep");
      // webrtcBlob.isInitiator = false;
      // gameBlob.state.player = 2;
      // gameBlob.state.otherPlayer = 1;
    }
    //readyOrRejoin();
  });

  socket.on('full', (frand) => {
    console.log("full");
  });

  socket.on('rejoin', (player, frand) => {
    console.log("create or join room rejoin");

    if(frand === 0) {
      console.log("reset");
      // gameBlob.state.player      = player;
      // gameBlob.state.otherPlayer = player === 1 ? 2 : 1;
      // webrtcBlob.isInitiator     = player === 1 ? true : false;

      // sendMessage({type: 'ayyyyy'}, gameBlob.state.guid);

      // if (localStorage.getItem("positions") !== null) {
      //   gameBlob.state.positions = JSON.parse(localStorage.getItem("positions"));
      // }

      // updatePositions();
      // readyOrRejoin();
    }
  });

  socket.on('dconn', (frand) => {
    if(frand !== gameBlob.state.frand_id) {
      console.warn(`connection lost, frand ${frand}`);
      // $("#p"+frand).hide();
      // $("#p"+frand+"-ghost").css("left", gameBlob.state.positions[frand].x);
      // $("#p"+frand+"-ghost").show();
      // $(".connection-warning").show();
    } else {
      console.warn(`got disconnect event for self...`);
    }
  });

  socket.on('move', (move, player) => {
    console.log("move");
    // gameBlob.state.positions[player] = JSON.parse(move);
    // updatePositions();
  });

  socket.on('message', () => console.log('message'));

  socket.on('log', (array) => {
    console.debug.apply(console, array);
  });
}
