const webpack = require("webpack");
const path = require('path');

const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const StyleExtHtmlWebpackPlugin = require('style-ext-html-webpack-plugin');

module.exports = {
  entry: ['./src/hergs.js', './src/hergs.css'],
  output: {
    filename: 'bundle.[hash].js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    loaders: [
      { // Run all js files through babel
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      },
      { // handle css files with ExtractTextPlugin
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader?-url', 'sass-loader']
        })
      }
    ],
  },
  plugins: [
    // Copy static content in public into the distribution
    new CopyWebpackPlugin([
      { from: 'public' }
    ]),

    // Inject our bundle reference into the template and generate index.html
    new HtmlWebpackPlugin({
      template: 'src/index.ejs',
      inject: 'body',
    }),

    // Combine all style resources into a 'styles.css'
    new ExtractTextPlugin('styles.[hash].css'),

    // Inline our styles.css into the HTML
    new StyleExtHtmlWebpackPlugin(),

    // for modules that use global variables
    new webpack.ProvidePlugin({
        io: 'socket.io-client',
    }),
  ],

  // Enable source maps
  devtool: 'source-map'
}

